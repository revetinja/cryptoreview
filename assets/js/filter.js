var rating = [];
var deposit = [];
var cryptoc = [];
var ratingRow = $('tr td:nth-child(3)');
var depositRow = $('tr td:nth-child(4)');
var cryptoRow = $('tr td:nth-child(2)');

for (var i = 0; i < ratingRow.length; i++) {
    rating.push(ratingRow[i].innerText);
    deposit.push(parseInt(depositRow[i].innerText));
    cryptoc.push(cryptoRow[i].childElementCount);
}

$('#userRating').on('change', function () {
    var selectedValue = this.selectedOptions[0].value;

    $("#minDeposit").val('minimum'); // Setting the value as minimum
    $('#cryptoNo').val('all');

    for (var i = 0; i < rating.length; i++) {
        if (selectedValue == 'all-user') {
            ratingRow[i].parentNode.style = "display:;";
        } else if (selectedValue > rating[i]) {
            ratingRow[i].parentNode.style = "display:none;";
        } else if (selectedValue <= rating[i]) {
            ratingRow[i].parentNode.style = "display:;";
        }
    }
});

$('#minDeposit').on('change', function () {
    var selectedDepositValue = this.selectedOptions[0].value;

    $("#userRating").val('all-user'); // Setting the value as 'all users'
    $('#cryptoNo').val('all');


    for (var i = 0; i < deposit.length; i++) {
        if (selectedDepositValue == 'minimum') {
            depositRow[i].parentNode.style = "display:;";
        } else if (selectedDepositValue >= deposit[i]) {
            depositRow[i].parentNode.style = "display:;";
        } else if (selectedDepositValue <= deposit[i]) {
            depositRow[i].parentNode.style = "display:none;";
        }
    }
});

$('#cryptoNo').on('change', function () {
    var selectedCryptoValue = this.selectedOptions[0].value;

    $("#userRating").val('all-user');
    $("#minDeposit").val('minimum');

    for (var i = 0; i < deposit.length; i++) {
        if (selectedCryptoValue == 'all') {
            cryptoRow[i].parentNode.style = "display:;";
        } else if (selectedCryptoValue <= cryptoc[i]) {
            cryptoRow[i].parentNode.style = "display:;";
        } else if (selectedCryptoValue >= cryptoc[i]) {
            cryptoRow[i].parentNode.style = "display:none;";
        }
    }
});