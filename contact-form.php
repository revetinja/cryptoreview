<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Contact us | Top 10 Crypto Brokers</title>

   <!-- Plugins -->
   <link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">
   <link rel="stylesheet" href="assets/css/vendor/animate.css">
   <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
   <link rel="stylesheet" href="assets/css/cryptocoins.css">

   <!-- Fonts -->
   <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

   <!-- Custom styles -->
   <link rel="stylesheet" href="assets/css/style.css">
</head>

<body id="top">

   <nav class="navbar fixed-top navbar-expand-lg navbar-dark custom-navbar">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03"
         aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="/">Top 10 Crypto Brokers</a>
      <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
         <div class="navbar-nav mr-auto mt-2 mt-lg-0">
         </div>
         <ul class="navbar-nav my-2 my-lg-0">
            <li class="nav-item">
               <a href="about.html" class="nav-link">About us</a>
            </li>
            <li class="nav-item">
               <span class="nav-link nav-link-custom">|</span>
            </li>
            <li class="nav-item">
               <a href="contact.html" class="nav-link">Contact</a>
            </li>
         </ul>
      </div>
   </nav>

   <header class="masthead mainhead text-center text-white">
      <div class="container">
         <div class="row">
            <div class="col-xl-9 mx-auto">
               <h1 class="slideText">
                  <span class="boldtext">CONTACT US</span>
               </h1>
            </div>
         </div>
      </div>
   </header>

   <div class="container" style="padding:30px 10px; border:1px solid rgba(255,255,255,0.5);">
         <div class="row">
            <div class="col-sm-12">
              <div class="text-center">
   
              <?php
if(isset($_POST['email'])) {
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "info@top10-crypto-brokers.com";
    $email_subject = "Contact Form";
 
    function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
        !isset($_POST['message'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
 
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
    $message = $_POST['message']; // required
 
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The Name you entered does not appear to be valid.<br />';
  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
     
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
    $email_message .= "Message: ".clean_string($message)."\n";
 
// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
?>
 
<!-- include your own success html here -->
 
Thank you for contacting us. We will be in touch with you very soon.
 
<?php
 
}
?>
              </div>
            </div>
         </div>
      </div>
      <footer id="footer">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 footer-text">
               <h5 class="text-center">RISK DISCLAIMER</h5>
               <p> Our site will not be held legally responsible for any loss or damage resulting from relying on the information
                  presented in this website, including brokers' reviews, financial news, authors' opinions and/or analysis.
                  The data included on our site is not always published in real-time and/or necessarily accurate and do not
                  always reflect the views of our site owners, employees and/or content contributors. Trading involves high
                  risks, and is not suitable for everyone. Before trading, one should be aware of the risks, know exactly
                  his investment goals and limits and acquire a fine level of risk understanding and risk management.</p>
               <br>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col text-center">
            <h2 class="copyright">Top 10 Crypto Brokers, 2018</h2>
         </div>
      </div>
   </footer>




   <div class="backtotop" id="backtotop" data-toggle="tooltip" data-placement="left" title="Back to Top">
      <i class="fa fa-angle-up arrow-up"></i>
   </div>

   <!-- Scripts -->
   <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.0.0/jq-3.2.1/dt-1.10.16/r-2.2.1/datatables.min.js"></script>
   <script type="text/javascript" src="assets/js/filter.js"></script>

   <script>
      $(document).ready(function () {
         $('#example').dataTable({
            "columnDefs": [{
               "orderable": false,
               "targets": [0, 0, 0, 0, 1, 4, 5] // Unorderable columns
            }],
            // Default sorting by rating
            // "aaSorting": [
            //    [2, 'des']
            // ],
            "searching": false,
            "lengthChange": false,
            "autoWidth": false
         });

         $(function () {
            $('[data-toggle="tooltip"]').tooltip()
         })
      });

      setTimeout(function () {
         $(".ccc-widget.ccc-chart a, .ccc-coin-header-v3-container a").css('pointer-events', 'none');
      }, 1500);
   </script>


   <script>
      $("#arrow").click(function () {
         $('html, body').animate({
            scrollTop: $("#crypto").offset().top
         }, 1000);
      });

      $("#backtotop").click(function () {
         $('html, body').animate({
            scrollTop: $("#top").offset().top
         }, 500);
      })
   </script>
</body>

